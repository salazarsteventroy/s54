class Queue
{

    constructor()
    {
        this.collection = [];
    }
                  

}

enqueue(element)
{    

    this.collection.push(element);
}

dequeue()
{

    if(this.isEmpty())
        return "Underflow";
    return this.items.shift();
}


front()
{

    if(this.isEmpty())
        return "No elements in Queue";
    return this.items[0];
}


isEmpty()
{

	return this.items.length == 0;
}


printQueue()
{
	var str = "";
	for(var i = 0; i < this.items.length; i++)
		str += this.items[i] +" ";
	return str;
}


var queue = new Queue();
              
  
// Testing dequeue and pop on an empty queue
// returns Underflow
console.log(queue.dequeue());
  
// returns true
console.log(queue.isEmpty());
  

queue.enqueue("John");
queue.enqueue("Jane");
queue.enqueue("Bob");
queue.enqueue("Cherry");


console.log(queue.front());
  

console.log(queue.dequeue());
  

console.log(queue.front());
  

console.log(queue.dequeue());

console.log(queue.printQueue());

module.exports = {
	collection,
	printQueue,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};